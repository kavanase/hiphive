Other functions
===============


.. _self_consistent_phonons:

.. index::
   single: Self-consistent phonons

Self-consistent phonons
-----------------------

.. automodule:: hiphive.self_consistent_phonons
   :members:
   :undoc-members:



.. index::
   single: Function reference; Utilities
   single: Class reference; Shell

MD tools
--------

.. autofunction:: hiphive.md_tools.spectral_energy_density.compute_sed
   :noindex:


Enforcing rotational sum rules
------------------------------

.. autofunction:: hiphive.core.rotational_constraints.enforce_rotational_sum_rules
   :noindex:


Utilities
---------

.. automodule:: hiphive.utilities
   :members:
   :undoc-members:
